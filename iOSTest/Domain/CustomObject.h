//
//  CustomObject.h
//  iOSTest
//
//  Created by Rafael Dick on 23/2/18.
//  Copyright © 2018 Rafael Dick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CustomObject : NSObject

@property (strong, nonatomic) NSString *navTitle;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *desc;
@property (strong, nonatomic) NSString *imgUrl;

- (instancetype)initWithName:(NSString *)title andDescription:(NSString *)desc andImgUrl:(NSString*)imgUrl;
- (void)setImageWithallback:(void (^)(NSData *data, NSError *error))callback;

@end
