//
//  CustomObject.m
//  iOSTest
//
//  Created by Rafael Dick on 23/2/18.
//  Copyright © 2018 Rafael Dick. All rights reserved.
//

#import "CustomObject.h"
#import "ObjectsManager.h"

@implementation CustomObject

- (instancetype)initWithName:(NSString *)title andDescription:(NSString *)desc andImgUrl:(NSString*)imgUrl {
    self = [super init];
    if (self) {
        self.title = title;
        self.desc = desc;
        self.imgUrl = imgUrl;
    }
    return self;
}

- (void)setImageWithallback:(void (^)(NSData *data, NSError *error))callback {
    if (![self.imgUrl isEqualToString:@""]) {
        [ObjectsManager getImageFromUrl:self.imgUrl callback:^(NSData *data, NSError *error) {
            callback(data, error);
        }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"TimeOut" code:0 userInfo:nil];
        callback(nil, error);
    }
}

@end


