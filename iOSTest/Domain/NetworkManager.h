//
//  NetworkManager.h
//  iOSTest
//
//  Created by Rafael Dick on 23/2/18.
//  Copyright © 2018 Rafael Dick. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject <NSURLConnectionDelegate>
{
    NSMutableData *_responseData;

}
@property (nonatomic, retain) NSString *url;

@property (nonatomic, retain) NSMutableData *responseData;

- (void) getDataWithNSURLConectionWithCallback:(void (^)(NSDictionary *result, NSError *error))callback;
- (void) getImageUsingURLConnectionWithUrl:(NSString *) url callback:(void (^)(NSData *data, NSError *error))callback;

- (void) getDataWithCallback:(void (^)(NSDictionary *dictionary, NSError *error))callback;

@end
