//
//  NetworkManager.m
//  iOSTest
//
//  Created by Rafael Dick on 23/2/18.
//  Copyright © 2018 Rafael Dick. All rights reserved.
//

#import "NetworkManager.h"

@implementation NetworkManager

@synthesize responseData = _responseData;
@synthesize url;

- (instancetype)init {
    self = [super init];
    if (self) {
        self.url = @"https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json";
    }
    return self;
}

/*
    NSURL are DEPRECATED - Implemented because is a test requesit;
 */
- (void) getDataWithNSURLConectionWithCallback:(void (^)(NSDictionary *dictionary, NSError *error))callback {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    
   [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
       if (response) {
           NSString *strISOLatin = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
           NSData *dataUTF8 = [strISOLatin dataUsingEncoding:NSUTF8StringEncoding];
           NSError *jsonError;
           NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dataUTF8 options:kNilOptions error:&jsonError];
           if(!jsonError) {
               callback(json, nil);
           }
       } else if(error != nil) {
           NSLog(@"%@", error);
           callback(nil, error);
       }
    }];
}

/*
 NSURL are DEPRECATED - Implemented because is a test requesit;
 */
- (void) getImageUsingURLConnectionWithUrl:(NSString *) url callback:(void (^)(NSData *data, NSError *error))callback {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (response) {
            callback(data, nil);
        } else if(error != nil) {
            callback(nil, error);
            NSLog(@"%@", error);
        }
    }];
}

/*
 NSURLSession  - Not requisit but implemented because is new way to do requests instead NSURLRequest;
 */
- (void) getDataWithCallback:(void (^)(NSDictionary *dictionary, NSError *error))callback {
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:self.url] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (response) {
            NSString *strISOLatin = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
            NSData *dataUTF8 = [strISOLatin dataUsingEncoding:NSUTF8StringEncoding];
            NSError *jsonError;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:dataUTF8 options:kNilOptions error:&jsonError];
            if(!jsonError) {
                callback(json, nil);
            }
        } else if(error != nil) {
            NSLog(@"%@", error);
        }
    }] resume];
}
@end
