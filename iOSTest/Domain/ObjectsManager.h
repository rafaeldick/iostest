//
//  ObjectsManager.h
//  iOSTest
//
//  Created by Rafael Dick on 23/2/18.
//  Copyright © 2018 Rafael Dick. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjectsManager : NSObject

+ (void) getData;
+ (void) getImageFromUrl:(NSString *) url callback:(void (^)(NSData *data, NSError *error))callback;

@end
