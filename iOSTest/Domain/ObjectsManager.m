//
//  ObjectsManager.m
//  iOSTest
//
//  Created by Rafael Dick on 23/2/18.
//  Copyright © 2018 Rafael Dick. All rights reserved.
//

#import "ObjectsManager.h"
#import "NetworkManager.h"
#import "CustomObject.h"

@implementation ObjectsManager

+ (void) getData {
    NetworkManager *network = [NetworkManager new];
    [network getDataWithNSURLConectionWithCallback:^(NSDictionary *result, NSError *error) {
        if (result != nil) {
            NSString *title = [result objectForKey:@"title"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateNavTitle" object:title];

            NSArray *array = [self convertToCustomObjects:[result objectForKey:@"rows"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"dataReceived" object:array];
        }
    }];
}

+ (void) getImageFromUrl:(NSString *) url callback:(void (^)(NSData *data, NSError *error))callback{
    NetworkManager *network = [NetworkManager new];
    [network getImageUsingURLConnectionWithUrl:url callback:^(NSData *data, NSError *error) {
        if (data) {
         callback(data, nil);
        } else {
            callback(nil, error);
        }
    }];
}

+ (NSMutableArray *) convertToCustomObjects:(NSArray *)array {
    NSMutableArray *arrayReturn = [NSMutableArray new];
    
    for (NSDictionary *dict in array) {
        CustomObject *obj = [CustomObject new];
        obj.title = [self isNil:[dict objectForKey:@"title"]] ? @"" : [dict objectForKey:@"title"];
        obj.desc =  [self isNil:[dict objectForKey:@"description"]] ? @"" : [dict objectForKey:@"description"];
        obj.imgUrl = [self isNil:[dict objectForKey:@"imageHref"]] ? @"" : [dict objectForKey:@"imageHref"];
        if (![self checkIsAllNil:obj]) {
            [arrayReturn addObject:obj];
        }
    }
    
    return  arrayReturn;
}

+ (BOOL) checkIsAllNil:(CustomObject *) object {
    
    if([self isNil:object.title] && [self isNil:object.desc] && [self isNil:object.imgUrl]) {
        return true;
    }
    
    return false;
}

+ (BOOL) isNil:(NSString *) str {
    
    if (str == nil || [str isKindOfClass:[NSNull class]] || [str isEqualToString:@""]){
        return true;
    }
    return false;
}
@end
