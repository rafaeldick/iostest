//
//  main.m
//  iOSTest
//
//  Created by Rafael Dick on 23/2/18.
//  Copyright © 2018 Rafael Dick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
