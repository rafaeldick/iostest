//
//  CustomTableViewCell.h
//  iOSTest
//
//  Created by Rafael Dick on 23/2/18.
//  Copyright © 2018 Rafael Dick. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomObject;
@interface CustomTableViewCell : UITableViewCell

@property (strong, nonatomic) UILabel *lblTitle;
@property (strong, nonatomic) UILabel *lblDesc;
@property (strong, nonatomic) UIImageView *img;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

- (void) createComponents;
- (void)configureCellWith:(CustomObject *)customobject;


@end
