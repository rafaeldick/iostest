//
//  CustomTableViewCell.m
//  iOSTest
//
//  Created by Rafael Dick on 23/2/18.
//  Copyright © 2018 Rafael Dick. All rights reserved.
//

#import "CustomTableViewCell.h"
#import "CustomObject.h"

@implementation CustomTableViewCell

- (void) createComponents {
    if (self.img == nil) {
        self.img = [[UIImageView alloc] init];
        [self.img setTranslatesAutoresizingMaskIntoConstraints:NO];
        self.lblTitle = [[UILabel alloc] init];
        self.lblDesc = [[UILabel alloc] init];
        self.lblTitle.textColor = [UIColor whiteColor];
        self.lblDesc.textColor = [UIColor whiteColor];
        [self.lblTitle setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.lblDesc setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.lblDesc sizeToFit];
        [self.lblTitle sizeToFit];
        self.lblTitle.textAlignment = NSTextAlignmentCenter;
        [self.lblTitle setNumberOfLines:0];
        [self.lblDesc setNumberOfLines:0];
        [self.contentView addSubview:self.img];
        [self.contentView addSubview:self.lblTitle];
        [self.contentView addSubview:self.lblDesc];
        
        self.activityIndicator = [[UIActivityIndicatorView alloc] init];
        self.activityIndicator.color = [UIColor whiteColor];
        [self.activityIndicator setTranslatesAutoresizingMaskIntoConstraints:NO];

        [self createImageConstraints];
        [self createLabelsConstraint];
        
        [self.img addSubview:self.activityIndicator];
        [self createActivityIndicatorConstraints];

    }
    
}

- (void)configureCellWith:(CustomObject *)customobject {
    self.lblTitle.text = customobject.title;
    self.lblDesc.text = customobject.desc;
    self.backgroundColor = [UIColor clearColor];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    if (self.img.image == nil) {
        [self.activityIndicator startAnimating];
        [customobject setImageWithallback:^(NSData *data, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.activityIndicator stopAnimating];
                if (error) {
                    self.img.image = [UIImage imageNamed:@"image not found"];
                } else {
                    self.img.image = [UIImage imageWithData:data];
                }
            });
        }];
    }
  
}

- (void) createActivityIndicatorConstraints {
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:75];
    
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:75];
    
    [self.activityIndicator addConstraints:@[height, width]];
    
    NSLayoutConstraint *centerHorizontal = [NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.img attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
    
    NSLayoutConstraint *centerVertical = [NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.img attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    
    [self.img addConstraints:@[centerHorizontal, centerVertical]];

}

- (void) createImageConstraints {
    
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.img attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:150];
    
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.img attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:150];
    
    [self.img addConstraints:@[height, width]];
    
    NSLayoutConstraint *centerHorizontal = [NSLayoutConstraint constraintWithItem:self.img attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
    
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.img attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:8];
    
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.img attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.lblTitle attribute:NSLayoutAttributeTop multiplier:1 constant:-5];
    
    [self.contentView addConstraints:@[centerHorizontal, top, bottom]];
}

- (void)createLabelsConstraint {
    NSLayoutConstraint *leftTitle = [NSLayoutConstraint constraintWithItem:self.lblTitle attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.contentView attribute:NSLayoutAttributeLeading multiplier:1 constant:8];
    
      NSLayoutConstraint *centerHorizontalTitle = [NSLayoutConstraint constraintWithItem:self.lblTitle attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
    
    NSLayoutConstraint *leftDesc = [NSLayoutConstraint constraintWithItem:self.lblDesc attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeading multiplier:1 constant:8];
    
    NSLayoutConstraint *rightDesc = [NSLayoutConstraint constraintWithItem:self.lblDesc attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTrailing multiplier:1 constant:-8];
    
    NSLayoutConstraint *topTitle = [NSLayoutConstraint constraintWithItem:self.lblTitle attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.img attribute:NSLayoutAttributeBottom multiplier:1 constant:5];
    
    NSLayoutConstraint *topDesc = [NSLayoutConstraint constraintWithItem:self.lblDesc attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.lblTitle attribute:NSLayoutAttributeBottom multiplier:1 constant:5];
    
    NSLayoutConstraint *bottomDesc = [NSLayoutConstraint constraintWithItem:self.lblDesc attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationLessThanOrEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1 constant:-8];
    
    [self.contentView addConstraints:@[centerHorizontalTitle, leftTitle, leftDesc, rightDesc, topTitle, topDesc, bottomDesc]];
}
@end
