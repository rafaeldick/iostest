//
//  MainViewController.m
//  iOSTest
//
//  Created by Rafael Dick on 23/2/18.
//  Copyright © 2018 Rafael Dick. All rights reserved.
//

#import "MainViewController.h"
#import "CustomTableViewCell.h"
#import "CustomObject.h"
#import "ObjectsManager.h"

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSArray *array;
@property (nonatomic) UINavigationBar* navbar;
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavBar];
    [self createTableView];
    
    [ObjectsManager getData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dataReceived:)
                                                 name:@"dataReceived"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateNavTitle:)
                                                 name:@"updateNavTitle"
                                               object:nil];
    
}

- (void) createNavBar {
    _navbar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 0, 50)];
    UINavigationItem* navItem = [[UINavigationItem alloc] initWithTitle:@""];
    [_navbar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    UIBarButtonItem *itemRefresh = [[UIBarButtonItem alloc] initWithTitle:@"Refresh" style:UIBarButtonItemStyleDone target:self action:@selector(refreshData)];
    _navbar.barTintColor = [UIColor redColor];
    itemRefresh.tintColor = [UIColor whiteColor];
    navItem.rightBarButtonItem = itemRefresh;
    [_navbar setItems:@[navItem]];
    
    [self.view addSubview:_navbar];
    [self addNavBarConstraints];
}

- (void)refreshData {
    [ObjectsManager getData];
}

- (void) updateNavTitle:(NSNotification *)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[_navbar.items objectAtIndex:0] setTitle:notification.object];
    });
}

- (void) dataReceived:(NSNotification *)notification {
    self.array = notification.object;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

-(void) createTableView {
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor redColor];
    self.tableView.allowsSelection = false;
    [self.tableView registerClass:[CustomTableViewCell class] forCellReuseIdentifier:@"cell"];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.view addSubview:self.tableView];
    [self addContraintTableView];
}

#pragma mark: UITableView Datasource and Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    CustomTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = (CustomTableViewCell *)[[CustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    [cell createComponents];
    [cell configureCellWith:self.array[indexPath.row]];
    
    return cell;
}

-(void) addNavBarConstraints {
    [_navbar setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:self.navbar attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:self.navbar attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.navbar attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:25];

    [self.view addConstraints:@[top, left, right]];
}

- (void) addContraintTableView {
    [self.tableView setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:70];
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    [self.view addConstraints:@[top, left, right, bottom]];
}

@end
